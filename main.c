#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdbool.h>
#include <string.h>
//Estructura de la informacion del tipo Usuario
typedef struct Usuario{
	char user_ID[50];
	char nombre[50];
	char descripcion[50];
	char home[50];
	char shell[50];
	struct Usuario* siguiente;
} usuario_t;


usuario_t *cabeceraUser = NULL;

//Variables globales


//Determinamiento si la opción -g de group está siendo usada
bool gflag = false; //Opción -g por defecto apagada
char *nombre_usuario, *nombre_grupo;

//Variables auxiliares
int contador = 0, controlador = 0;


//Archivos a Leer
FILE *fpLecturaUser;
FILE *fpLecturaGroup;

//Enlazar los usuarios en la lista de Usuarios
usuario_t *enlaceUser, *usuario_siguiente;

//Declaracion de funciones a usar
void informacionUsuarios();
void recorrer_usuarios();
void buscarInformacionUsuarios();
char *nuevoStrtok();

int main (int argc, char *argv[])
{
	//opcion ingresada con -
	int opt;

	//funcion getopt
	while ((opt = getopt (argc, argv, "gh")) != -1){
		switch(opt)
		{
			//para saber que se trabajara con grupo
			case 'g':
				gflag = true;
				break;

			//para mostrar mensaje de ayuda
			case 'h':
				printf( "info [opcion] USUARIO|GRUPO\n");
				printf( "-Muestra la informacion del USUARIO ingresado\n");
				printf( "-h: ayuda, muestra este mensaje\n");
				printf( "-g GRUPO: muestra la información de todos los usuarios en GRUPO\n");
				return 0;

			//para errores al ingresar opcion
			case '?':
				printf( "Comando no existente\n\n");
				printf( "info [opcion] USUARIO|GRUPO\n");
				printf( "-Muestra la informacion del USUARIO ingresado\n");
				printf( "-h: ayuda, muestra este mensaje\n");
				printf( "-g GRUPO: muestra la información de todos los usuarios en GRUPO\n");
				return 0;
			default:
				fprintf(stderr, "estructura de comando: %s [-h] [-g] USUARIO|GRUPO\n", argv[0]);
				return -1;
		}
	}

	//si se trabaja con grupo, se asigna a grupo. 1er valor ejecutable, 2do opcion, 3ero nombre de grupo (argv[2])
	if(gflag && argc==3)
		nombre_grupo = argv[2];

	//no se trabaja con grupo, se usara el segundo argumento como nombre de usuario
	else if(!gflag && argc==2)
		nombre_usuario = argv[1];

	//para detener errores de ingresos por numero de argumentos erroneo
	else{
		printf( "Numero de argumentos proporcionado es erroneo\n\n");
		printf( "info [opcion] USUARIO|GRUPO\n");
		printf( "-Muestra la informacion del USUARIO ingresado\n");
		printf( "-h: ayuda, muestra este mensaje\n");
		printf( "-g GRUPO: muestra la información de todos los usuarios en GRUPO\n");
		return -1;
	}


	
	//Opcion para encontrar usuarios en el sistema
	if(gflag == false){
		informacionUsuarios();
		recorrer_usuarios();
	}

	else{
		//Obtenemos la informacion del /etc/group a un archivo Grupos.txt
		system("cat /etc/group > Grupos.txt");

		char buffer[1024] = "";
		char *token;
		char *tokenU;
		bool flag = true;
		fpLecturaGroup = fopen("Grupos.txt","r");

		//Funcion para obtener la informacion de todos los usuarios del sistema
		informacionUsuarios();

		//Leemos el archivo Grupos.txt y lo recorremos, haciendo un "split" (con strtok) con ":"
		while(fgets(buffer,1024,(FILE*)fpLecturaGroup)){
			token = strtok(buffer,":");

			//Buscamos el grupo que ingresamos en el comando
			if(strcmp(nombre_grupo,token) == 0){
				flag = false;
				int count = 0;

				//Recorremos los valores de dicho grupo
				while(token != NULL){

					//Obtenemos la informacion de los usuarios del grupo
					if(count == 3){
						if(strlen(token) > 1){
							printf("Miembros del grupo %s:\n\n",nombre_grupo);
							tokenU = strtok(token,",");
							/*Recorremos los usuarios que existen en dicho grupo y obtenemos la informacion 								con la funcion para buscarInformacionUsuarios();
							*/
							while(tokenU != NULL){
								buscarInformacionUsuarios(tokenU);
								tokenU = strtok(NULL,",");
							}
							
						}else{
							fprintf(stderr, "No hay usuarios en el grupo %s\n", nombre_grupo);
						}
					}
					token = strtok(NULL,":");
					count+=1;
				}	
			}
			
		}
		if(flag)
			fprintf(stderr, "El grupo %s no existe\n", nombre_grupo);
		fclose(fpLecturaGroup);
	}			
}


/*
Funcion para recorrer la lista de usuarios y obtener la informacion solicitada
	No recibe parametros y no retorna nada, pues es void
*/
void recorrer_usuarios(){
	usuario_t *ptr;
	ptr = cabeceraUser;
	int i = 0 ;
	bool flag = true;
	while(ptr != NULL && i<controlador) {
		if(strcmp(ptr->nombre,nombre_usuario) == 0){
			printf("Informacion de %s\n", ptr->nombre);
			printf("UID: %s\n", ptr->user_ID);
			printf("Descripcion: %s\n", ptr->descripcion);
			printf("Home: %s\n", ptr->home);
			printf("Shell: %s\n", ptr->shell);
			flag = false;
		}
		ptr = ptr->siguiente;
		i=i+1;
	}
	if(flag)
		fprintf(stderr, "El usuario %s no existe en el sistema\n", nombre_usuario);
}



/*
Funcion para recorrer la lista de usuarios cuando queremos obtener la informacion de varios usuarios
	Recibe un parametro usuario de tipo char* y no retorna nada, pues es void
*/
void buscarInformacionUsuarios(char* usuario){
	usuario_t *ptr;
	ptr = cabeceraUser;
	int i = 0 ;
	bool flag = true;
	while(ptr != NULL && i<controlador) {
		if(strncmp(ptr->nombre,usuario,strlen(ptr->nombre)) == 0){
			printf("Informacion de %s\n", ptr->nombre);
			printf("UID: %s\n", ptr->user_ID);
			printf("Descripcion: %s\n", ptr->descripcion);
			printf("Home: %s\n", ptr->home);
			printf("Shell: %s\n", ptr->shell);
		}
		ptr = ptr->siguiente;
		i=i+1;
	}
}


/*
Funcion que permite obtener la informacion de los usuarios en una lista enlazada
	No recibe parametros ni devuelve nada, pues es funcion void
*/
void informacionUsuarios(){

	//Obtenemos la informacion del /etc/passwd (solo de usuarios) a un archivo Usuarios.txt
	system("cat /etc/passwd > Usuarios.txt");


	char buffer[1024] = "";
	char *token;
	int count = 0;
	fpLecturaUser = fopen("Usuarios.txt","r");

	//Lectura del archivo Usuarios.txt y recorrido del mismo
	while(fgets(buffer,1024,(FILE*)fpLecturaUser)){
	controlador = controlador + 1;
		
		//Reserva de espacio de memoria para la lista enlazada
		if (contador == 0){
			enlaceUser = (usuario_t *) calloc(1,sizeof(usuario_t));
		} else{
			enlaceUser = usuario_siguiente;
		}
		usuario_siguiente = (usuario_t *) calloc(1,sizeof(usuario_t));
		token = nuevoStrtok(buffer,":");
		//Recorrer parametros y extraer la informacion para ingresarla a la lista enlazada
		while(token!=NULL){
			contador = contador+1;
			if(count == 0){
				strncpy(enlaceUser->nombre,token,50);
			}
			if(count == 2){
				strncpy(enlaceUser->user_ID,token,50);
			}
			if(count == 4){
				strncpy(enlaceUser->descripcion,token,50);
			}
			if(count == 5){
				strncpy(enlaceUser->home,token,50);
			}
			if(count == 6){
				strncpy(enlaceUser->shell,token,50);
			}
			count = count + 1;
			token = nuevoStrtok(NULL,":");
			if(count == 7){
				count = 0;
				if (contador == 7) {
					cabeceraUser = enlaceUser;
				}					
			}
		}
		//Hacemos el enlace con el siguiente nodo
		enlaceUser->siguiente = usuario_siguiente;		
	}
	fclose(fpLecturaUser);	
}

//Implementacion de una nueva funcion similar a la strtok para contar los espacios en blanco
char *nuevoStrtok(char * cadena, char const * limitador){
	char *p, *valorRetorno = 0;
   	static char *control = NULL;

	//Se asigna una cadena con "limitador" para que sea recorrida 
   	if(cadena != NULL)       
			control = cadena;
   	if(control == NULL)      
			return NULL;
	
	//Obtiene las palabras hasta encontrar el siguiente ":" y retorna la palabra hallada
   	if((p = strpbrk (control, limitador)) != NULL) {
      		*p  = 0;
      		valorRetorno = control;
      		control = ++p;
   	}
	//Retorna el ultimo valor, que seria el mensaje del Shell
	else{
		valorRetorno = control;
		control = NULL;
		return valorRetorno;
   	}
	return valorRetorno;
}
