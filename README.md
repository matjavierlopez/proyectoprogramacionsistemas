# ProyectoProgramacionSistemas #

### Integrantes ###

* LEONARDO DAVID GOMEZ PESANTES
* MATHEUS JAVIER LOPEZ VELEZ

### Como usar el programa ###
* Se usara el comando "./info nombre_usuario" o "./info -g nombre_grupo", cualquier otro comando no sera aceptado y presentara el mensaje de como se debe usar el comando.
* No se aceptan 2 o mas usuarios o grupos de manera simultanea. Ej: ./info u1 u2... un (en caso de usuarios) y ./info -g g1 g2... gn (en caso que sean grupos). De igual forma, se presentara el comando de ayuda en este caso.
* Existen campos que no poseen informacion en ciertos usuarios (generalmente la descripcion de los usuarios especiales), esos campos se presentaran vacios cuando se imprima por pantalla la informacion.

### Notas ###
* El archivo Usuarios.txt se genera automaticamente en cada equipo, no es un archivo predeterminado.
* El archivo Grupos.txt se genera automaticamente en cada equipo, no es un archivo predeterminado.
* Se tomara en cuenta a todos los usuarios del sistema, sin excepcion alguna.
* Se tomara en cuenta a todos los grupos que existen en el sistema, sin excepcion alguna.

 

